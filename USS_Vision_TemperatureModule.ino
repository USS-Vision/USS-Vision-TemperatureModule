// DHT Temperature & Humidity Sensor
// Unified Sensor Library Example
// Written by Tony DiCola for Adafruit Industries
// Released under an MIT license.

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include "Secrets.h"



// time of last push
unsigned long lastTime = 0;
unsigned long lastTimeRandomEvent = 0;
// Delay between pushes in ms
unsigned long timerDelay = 10000;
unsigned long timerDelayRandomEvent = 10000;

#define DHTPIN 5 // Digital pin connected to the DHT sensor
#define RPIN 21
#define GPIN 22
#define BPIN 23
#define NUM_COLORS 5

#define DHTTYPE DHT22 // DHT 22 (AM2302)

DHT_Unified dht(DHTPIN, DHTTYPE);

uint32_t delayMS;

void setup()
{
  Serial.begin(9600);
  // setup LEDS
  pinMode(RPIN, OUTPUT);
  pinMode(GPIN, OUTPUT);
  pinMode(BPIN, OUTPUT);

  redLight();
  // Initialize device.
  dht.begin();

  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print(F("Sensor Type: "));
  Serial.println(sensor.name);
  Serial.print(F("Driver Ver:  "));
  Serial.println(sensor.version);
  Serial.print(F("Unique ID:   "));
  Serial.println(sensor.sensor_id);
  Serial.print(F("Max Value:   "));
  Serial.print(sensor.max_value);
  Serial.println(F("°C"));
  Serial.print(F("Min Value:   "));
  Serial.print(sensor.min_value);
  Serial.println(F("°C"));
  Serial.print(F("Resolution:  "));
  Serial.print(sensor.resolution);
  Serial.println(F("°C"));
  Serial.println(F("------------------------------------"));
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  Serial.println(F("Humidity Sensor"));
  Serial.print(F("Sensor Type: "));
  Serial.println(sensor.name);
  Serial.print(F("Driver Ver:  "));
  Serial.println(sensor.version);
  Serial.print(F("Unique ID:   "));
  Serial.println(sensor.sensor_id);
  Serial.print(F("Max Value:   "));
  Serial.print(sensor.max_value);
  Serial.println(F("%"));
  Serial.print(F("Min Value:   "));
  Serial.print(sensor.min_value);
  Serial.println(F("%"));
  Serial.print(F("Resolution:  "));
  Serial.print(sensor.resolution);
  Serial.println(F("%"));
  Serial.println(F("------------------------------------"));
  // Set delay between sensor readings based on sensor details.
  delayMS = sensor.min_delay / 1000;

  blueLight();
  connectWifi();

  Serial.println("Timer set to 5 seconds " + (String)(timerDelay / 1000) + " seconds.");
  greenLight();
  delay(300);
  randomColor();
}
int lastError = 0;
void loop()
{
  unsigned long curTime = millis();
  // delay between color changes
  if ((curTime - lastTime) > timerDelay && lastError == 0)
  {
    randomColor();
  }

  // delay between random Events
  if ((curTime - lastTimeRandomEvent) > timerDelayRandomEvent && lastError == 0)
  {
    doRandomEvent();
  }

  // Delay between measurements.
  if ((curTime - lastTime) > timerDelay)
  {
    // Read temperature and humidity
    sensors_event_t event;
    dht.temperature().getEvent(&event);
    if (isnan(event.temperature))
    {
      Serial.println(F("Error reading temperature!"));
      redLight();
      lastError = 1;
    }
    else
    {
      Serial.print(F("Temperature: "));
      Serial.print(event.temperature);
      Serial.println(F("°C"));
      lastError = 0;
    }
    // Get humidity event and print its value.
    dht.humidity().getEvent(&event);
    if (isnan(event.relative_humidity))
    {
      Serial.println(F("Error reading humidity!"));
      redLight();
      lastError = 1;
    }
    else
    {
      Serial.print(F("Humidity: "));
      Serial.print(event.relative_humidity);
      Serial.println(F("%"));
      lastError = 0;
    }

    // Check WiFi connection status
    if (WiFi.status() == WL_CONNECTED)
    {
      // Push Temperature and Humidity to VV
      setStateVector("Temperature", String(event.temperature));
      setStateVector("Humidity", String(event.relative_humidity));
    }
    else
    {
      Serial.println("WiFi Disconnected. Reconnecting");
      connectWifi();
    }
    lastTime = millis();
  }
}

bool setStateVector(String state, String value)
{
  HTTPClient http;
  http.setTimeout(10);

  String serverPath = serverName + "?Description=" + state + "&Flag=" + value;

  // Connect to VV
  http.begin(serverPath.c_str());
  Serial.println(serverPath.c_str());

  // Send xmlhttp GET request
  int httpResponseCode = http.GET();

  if (httpResponseCode > 0)
  {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    String payload = http.getString();
    Serial.println(payload);
    return true;
  }
  else
  {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
    return false;
  }
  // Free resources
  http.end();
}

void connectWifi()
{

  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}

void redLight()
{
  setColor(120, 0, 0);
}
void blueLight()
{
  setColor(0, 0, 120);
}
void greenLight()
{
  setColor(0, 120, 0);
}
void yellowLight()
{
  setColor(60, 60, 0);
}
void purpleLight()
{
  setColor(60, 0, 60);
}
void lightOff()
{
  setColor(0, 0, 0);
}

void randomColor()
{
  int color = random(NUM_COLORS);

  switch (color)
  {
  case 0:
    redLight();
    break;
  case 1:
    blueLight();
    break;
  case 2:
    greenLight();
    break;
  case 3:
    purpleLight();
    break;
  case 4:
    yellowLight();
    break;
  }
}

void doRandomEvent()
{
  timerDelayRandomEvent = random(7000, 30000);
  int blinkAmount = random(1, 5);
  for (int i = 0; i < blinkAmount; i++)
  {
    randomColor();
    delay(random(100, 300));
  }
  lastTimeRandomEvent = millis();
}
void setColor(int R, int G, int B)
{
  analogWrite(RPIN, 255 - R);
  analogWrite(GPIN, 255 - G);
  analogWrite(BPIN, 255 - B);
}
